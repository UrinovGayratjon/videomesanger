package uz.urinovsoft.ugram

import android.content.Context
import android.content.SharedPreferences

object PreferencesHelper {


    private const val USER_NAME = "NAME"

    private lateinit var preferences: SharedPreferences

    fun init(context: Context) {
        preferences = context.getSharedPreferences("Ugram", Context.MODE_PRIVATE)
    }

    fun clear() {
        userName = null
    }



    var userName: String?
        get() = preferences.getString(USER_NAME, "")
        set(value) {
            preferences.edit().putString(USER_NAME, value).apply()
        }
}